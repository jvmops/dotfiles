#!/usr/bin/env bash
# installs everything what I currently need

# some helper functions
function prompt() {
  echo "dotfiles :: plugins :: Do you want to $1? [Y/n]"
  read response
  [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
}

function install_from() {
  file="$file"
  echo "dotfiles :: plugins :: Source: $file"
  if [[ -d "$file" ]]; then
    install_from_directory "$file"
  else
    install_from_bash_file "$file"
  fi
}

function install_from_directory() {
  plugin_name=$(basename "$1")
  if prompt "install $plugin_name"; then
    bash "$1/install.bash"
  fi
}

function install_from_bash_file() {
  filename=$(basename "$1")
  plugin_name="${filename%.*}"
  if prompt "install $plugin_name"; then
    bash "$1"
  fi
}

# create autostart directory if it does not exist
mkdir -p "$HOME/.config/autostart"

if prompt "update apt"; then
   sudo apt-get update && sudo -K
   echo ""
   echo "dotfiles :: apt-get update complete"
fi

# install every plugin from ./plugins directory
for file in "./plugins"/*
do
  install_from "$file"
done

echo "setup :: Done. GG NO RE."