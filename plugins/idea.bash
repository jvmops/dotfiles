#!/usr/bin/env bash

# powerline fonts for cool effect:
#  - git clone https://github.com/powerline/fonts.git
#    ./install.sh
# variables:
idea_version=2019.2.4-no-jbr
idea_tarball=ideaIC-"$idea_version".tar.gz
idea_url=https://download.jetbrains.com/idea/"$idea_tarball"
idea_tmp=/tmp/idea
idea_home="$HOME/tools/intellij"

# script
sudo mkdir -p "$idea_tmp" "$idea_home" \
  && echo "dotfiles :: Intellij :: directories created" \
  && sudo chown "$(whoami)" "$idea_home" "$idea_tmp" \
  && sudo -K \
  && echo "dotfiles :: Intellij :: downloading $idea_version tarball" \
  && wget -N --directory-prefix "$idea_tmp" "$idea_url" \
  && tar -xvf "$idea_tmp"/"$idea_tarball" --directory "$idea_home" --strip 1 \
  && echo "dotfiles :: Intellij :: tarball extracted to: $idea_home" \
  && rm -fr "$idea_tmp"
