#!/usr/bin/env bash
# curl - command line tool for file transfer with a URL syntax
# htop - monitoring
# keychain - nice tool for handling rsa keys (python-pip - dependency)
# tree - directory tree view
sudo apt-get update && sudo apt-get --assume-yes install \
  curl \
  htop \
  firefox \
  python-pip \
  keychain \
  tree
