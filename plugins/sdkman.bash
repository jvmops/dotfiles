#!/usr/bin/env bash

curl -s "https://get.sdkman.io" | bash \
 && source "$HOME/.sdkman/bin/sdkman-init.sh" \
 && source "$HOME/.bashrc" \
 && sdk install java 11.0.5.hs-adpt \
 && sdk install java 13.0.1.hs-adpt \
 && sdk install java 8.0.232.hs-adpt \
 && sdk default java 13.0.1.hs-adpt \
 && sdk use java 13.0.1.hs-adpt \
 && sdk install kotlin \
 && sdk install groovy \
 && sdk install maven
