#!/usr/bin/env bash
# tig - git log viewer
sudo apt-get install git \
  tig

echo "dotfiles :: git :: git and tig installed"

cp .gitconfig ~/

read -rp "Enter your git email: " git_email
read -rp "Enter your git username: " git_username
git config --global user.email "$git_email" \
  && git config --global user.name "$git_username"
