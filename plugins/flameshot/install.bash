#!/usr/bin/env bash
# flameshot + autostart

# https://stackoverflow.com/a/8290652/4744090
# for gui run:
# $ gnome-session-properties
function link_autostart_file() {
  dot_desktop_file="$(pwd)/autostart/flameshot.desktop"
  echo "dotfiles :: Flameshot :: autostart source file [ $dot_desktop_file ]"
  ln -sf "$dot_desktop_file" "$HOME/.config/autostart/"
}

# only for gnome
# in ubuntu 19.04 there is a simpler syntax
function dotfiles_keyboard_shortcut() {
  if current_desktop_is_gnome; then
    gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot '' \
      && gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']" \
      && gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name 'flameshot' \
      && gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command '/usr/bin/flameshot gui' \
      && gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding 'Print'
  else
    echo "dotfiles :: Flameshot :: unknown deskop, you need to remap your 'prt sc' key yourself"
    exit 1
  fi
}

function current_desktop_is_gnome() {
  current_desktop=$(echo "$XDG_CURRENT_DESKTOP" | awk -F':' '{print tolower($2)}')
  echo "dotfiles :: Flameshot :: current_desktop=$current_desktop"
  [[ "gnome" == "$current_desktop" ]]
}

# installation
echo "dotfiles :: Flameshot :: installing flameshot will require sudo permissions"
sudo apt-get install flameshot && sudo -K \
  && link_autostart_file && echo "dotfiles :: Flameshot :: autostart link created" \
  && dotfiles_keyboard_shortcut && echo "dotfiles :: Flameshot :: keyboard shortcut mapped to regular 'prt sc' key"
