#!/usr/bin/env bash

# variables
chrome_url=https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
chrome_filename="google-chrome-stable_current_amd64.deb"
temp_dir="/tmp/chrome"
chrome_deb="$temp_dir"/"$chrome_filename"

# script part
mkdir --parents "$temp_dir" \
 && sudo apt-get --assume-yes install \
      libxss1 \
      libappindicator1 \
      libindicator7 \
 && wget --directory-prefix="$temp_dir" "$chrome_url" \
 && sudo dpkg -i "$chrome_deb" \
 && rm -fr "$temp_dir"
