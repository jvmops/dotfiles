#!/usr/bin/env bash
install_script="https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh"

sudo apt-get install zsh \
  && sudo apt-get install git-core \
  && wget "$install_script" -O - | zsh \
  && chsh -s "$(command -v zsh)" \
  && sudo shutdown -r 0
