#!/usr/bin/env bash

# yakuake + skins + autostart
# yakuake - quake like dropdown terminal
#     fav theme: Breezly by Andrei Shevchuk
#     https://github.com/shvchk/breezly-yakuake
#     https://github.com/shvchk/breezly-dark

function link_autostart_file() {
  dot_desktop_file="$(pwd)/autostart/yakuake.desktop"
  echo "dotfiles :: Yakuake :: autostart file source [ $dot_desktop_file ]"
  ln -sf "$dot_desktop_file" "$HOME/.config/autostart/"
}

function copy_skins() {
  source="$(pwd)/plugins/yakuake/skins"
  echo "dotfiles :: Yakuake :: skins :: source: $source/*"
  target="/usr/share/yakuake/skins/"
  echo "dotfiles :: Yakuake :: skins :: target: $target"
  echo "dotfiles :: Yakuake :: skins :: copy will require sudo permissions for accessing yakuake skins home directory!"
  sudo rsync --recursive "$source"/* "$target" && sudo -K
}

# installation
echo "dotfiles :: Yakuake :: installing yakuake will require sudo permissions"
sudo apt-get install yakuake && sudo -K \
  && link_autostart_file \
  && copy_skins
