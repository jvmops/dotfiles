# dotfiles
This automates my ubuntu setup. Just run:
```
git clone https://github.com/jvmops/github-dotfiles.git
cd github-dotfiles
./install.bash
```

## content:
Everything is under `./plugins/` directory. Basically there are bash scripts that install: 
- [IntelliJ IDEA](https://www.jetbrains.com/idea/download/other.html) - community edition, I prefer [versions without JDK](https://github.com/jvmops/jvmops-dotfiles/blob/master/plugins/idea.bash), because of...
- [SDKMAN](https://sdkman.io/) - with [pre installed](https://github.com/jvmops/jvmops-dotfiles/blob/master/plugins/sdkman.bash) maven, few versions of jdk, groovy and kotlin.
- [Some simple apt installations](https://github.com/jvmops/jvmops-dotfiles/blob/master/plugins/tools_from_apt.bash) - firefox, htop, curl, etc
- [Flameshot](https://flameshot.js.org/#/) - screen saver with imgur integration, `prt sc` key works only for gnome 
- [Git setup](https://github.com/jvmops/jvmops-dotfiles/blob/master/plugins/git/install.bash) - ./gitconfig, some aliases and other utilities 
- [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh) with [my custom configuration](https://github.com/jvmops/jvmops-dotfiles/tree/master/plugins/oh-my-zsh)
- [Yakuake](https://en.wikipedia.org/wiki/Yakuake) with [my fav skins](https://github.com/jvmops/jvmops-dotfiles/tree/master/plugins/yakuake)
